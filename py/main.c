#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#define and &&
#define or ||

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

/* cube coords */
const static int directions[6][3] = {
    {+1, -1, 0}, {+1, 0, -1}, {0, +1, -1},
    {-1, +1, 0}, {-1, 0, +1}, {0, -1, +1}
};

typedef struct s_cube_coords {
    int x;
    int y;
    int z;
} t_cube_coords;

t_cube_coords new_cube_coords(int x, int y, int z)
{
    t_cube_coords c = (t_cube_coords){x, y, z};
    return c;
}

int cube_coords_equal(t_cube_coords a, t_cube_coords b)
{
    return (a.x == b.x and a.y == b.y and a.z == b.z);
}

t_cube_coords cube_coords_add(t_cube_coords a, t_cube_coords b)
{
    t_cube_coords c = (t_cube_coords){a.x+b.x, a.y+b.y, a.z+b.z};
    return c;
}

t_cube_coords cube_coords_neighbor_with_distance(t_cube_coords a, int direction, int distance)
{
    t_cube_coords c = a;
    c.x += directions[direction][0] * distance;
    c.y += directions[direction][1] * distance;
    c.z += directions[direction][2] * distance;
    return c;
}

t_cube_coords cube_coords_neighbor(t_cube_coords a, int direction)
{
    return cube_coords_neighbor_with_distance(a, direction, 1);
}

int cube_coords_distance(t_cube_coords a, t_cube_coords b)
{
    return (abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z)) / 2;
}

t_cube_coords opposite(t_cube_coords a)
{
    t_cube_coords c = (t_cube_coords){-a.x, -a.y, -a.z};
    return c;
}

int i_max(int a, int b)
{
    if (a > b)
        return a;
    return b;
}

int i_min(int a, int b)
{
    if (a < b)
        return a;
    return b;
}

t_cube_coords *cube_coords_range(t_cube_coords a, int n)
{
    int len = sizeof(int) + sizeof(t_cube_coords) * 37;
    t_cube_coords *ptr = malloc(len);
    bzero(ptr, len);
    int index = 0;
    for (int x = -n; x <= n; x++)
    {
        for (int y = i_max(-n, -x - n); y <= i_min(n, -x + n); y++)
        {
            int z = -x - y;
            ptr[index++] = cube_coords_add(a, new_cube_coords(x, y, z));
        }
    }
    *((int *)(ptr)) = 0;
    return ptr;
}

int cube_coords_hash(t_cube_coords a)
{
    int prime = 31;
    int result = 1;
    result = prime * result + a.x;
    result = prime * result + a.y;
    result = prime * result + a.z;
    return result;
}

t_cube_coords index_to_cube(int index)
{
    const static int board[37][3] = {
            {0, 0, 0}, {1, -1, 0}, {1, 0, -1}, {0, 1, -1},
            {-1, 1, 0}, {-1, 0, 1}, {0, -1, 1}, {2, -2, 0},
            {2, -1, -1}, {2, 0, -2}, {1, 1, -2}, {0, 2, -2},
            {-1, 2, -1}, {-2, 2, 0}, {-2, 1, 1}, {-2, 0, 2},
            {-1, -1, 2}, {0, -2, 2}, {1, -2, 1}, {3, -3, 0},
            {3, -2, -1}, {3, -1, -2}, {3, 0, -3}, {2, 1, -3},
            {1, 2, -3}, {0, 3, -3}, {-1, 3, -2}, {-2, 3, -1},
            {-3, 3, 0}, {-3, 2, 1}, {-3, 1, 2}, {-3, 0, 3},
            {-2, -1, 3}, {-1, -2, 3},{0, -3, 3}, {1, -3, 2}, {2, -3, 1}
            };
    return new_cube_coords(board[index][0], board[index][1], board[index][2]);
}

int cube_to_index(t_cube_coords a)
{
    int hash = cube_coords_hash(a);
    if (hash == 29791) return 0;
    if (hash == 30721) return 1;
    if (hash == 30751) return 2;
    if (hash == 29821) return 3;
    if (hash == 28861) return 4;
    if (hash == 28831) return 5;
    if (hash == 29761) return 6;
    if (hash == 31651) return 7;
    if (hash == 31681) return 8;
    if (hash == 31711) return 9;
    if (hash == 30781) return 10;
    if (hash == 29851) return 11;
    if (hash == 28891) return 12;
    if (hash == 27931) return 13;
    if (hash == 27901) return 14;
    if (hash == 27871) return 15;
    if (hash == 28801) return 16;
    if (hash == 29731) return 17;
    if (hash == 30691) return 18;
    if (hash == 32581) return 19;
    if (hash == 32611) return 20;
    if (hash == 32641) return 21;
    if (hash == 32671) return 22;
    if (hash == 31741) return 23;
    if (hash == 30811) return 24;
    if (hash == 29881) return 25;
    if (hash == 28921) return 26;
    if (hash == 27961) return 27;
    if (hash == 27001) return 28;
    if (hash == 26971) return 29;
    if (hash == 26941) return 30;
    if (hash == 26911) return 31;
    if (hash == 27841) return 32;
    if (hash == 28771) return 33;
    if (hash == 29701) return 34;
    if (hash == 30661) return 35;
    if (hash == 31621) return 36;
    fprintf(stderr, "error: cube_to_index\n");
    abort();
}

typedef struct s_cell {
    int index;
    int richness;
} t_cell;

typedef struct s_tree {
    int index;
    int size;
    int is_mine;
    int is_dormant;
} t_tree;

enum {
    GROW,
    SEED,
    COMPLETE,
    WAIT
};

typedef struct s_action {
    int action;
    int arg1;
    int arg2;
} t_action;

typedef struct s_state {
    int day;
    int my_sun;
    int op_sun;
    int my_score;
    int op_score;
    int is_op_waiting;
    int nutrients;
    int len_trees;
    int len_cells;
    int len_possible_actions;
    t_tree *trees;
    t_cell *cells;
    t_action *possible_actions;
} t_state;

t_cell *new_cells()
{
    int len = 37 * sizeof(t_cell);
    t_cell *ptr = malloc(len);
    bzero(ptr, len);
    return (ptr);
}

t_state *new_state()
{
    t_state *state = malloc(sizeof(t_state));
    bzero(state, sizeof(t_state));

    state->possible_actions = malloc(200 * sizeof(t_action));
    bzero(state->possible_actions, 200 * sizeof(t_action));

    state->trees = malloc(37 * sizeof(t_tree));
    bzero(state->trees, 37 * sizeof(t_tree));
    state->trees[0].index = -1; // we use tree.index == cell_index to check if tree exists

    state->cells = malloc(37 * sizeof(t_cell));
    bzero(state->cells,37 * sizeof(t_cell));

    return state;
}

t_action *generate_possible_actions(t_state *state)
{
    int len = sizeof(int) + 200 * sizeof(t_action);
    void *ptr = malloc(len);
    bzero(ptr, len);

    int index = 0;
    t_action *possible_actions = ptr+sizeof(int);

    // we can always wait
    possible_actions[index++] = (t_action){WAIT, 0, 0};

    int seeds = 0;
    int size_1_trees = 0;
    int size_2_trees = 0;
    int size_3_trees = 0;
    for (int i = 0; i < state->len_trees; i++)
    {
        t_tree tree = state->trees[i];
        if (tree.is_mine == 0)
            continue;
        if (tree.index != i)
            continue;
        if (tree.size == 0) {
            seeds++;
        }
        else if (tree.size == 1) {
            size_1_trees++;
        }
        else if (tree.size == 2) {
            size_2_trees++;
        }
        else if (tree.size == 3) {
            size_3_trees++;
        }
        else {
            fprintf(stderr, "error: generate_possible_actions\n");
            abort();
        }
    }
    for (int i = 0; i < state->len_trees; i++)
    {
        t_tree tree = state->trees[i];

        void *ptr = cube_coords_range(index_to_cube(tree.index), tree.size);
        t_cube_coords *cube_coords_array = ptr + sizeof(int);
        int len = *((int*)(ptr));
        for (int j = 0; j < len; j++)
        {
            t_cube_coords c = cube_coords_array[j];
            t_cube_coords center = new_cube_coords(0,0,0);
            if (cube_coords_distance(c, center) > 3) {
                continue;
            }

            int cell_index = cube_to_index(c);

            if (state->cells[cell_index].richness == 0)
            {
                continue;
            }

            // tree exists
            if (state->trees[cell_index].index == cell_index)
            {
                continue;
            }
            // ok
            possible_actions[index++] = (t_action){SEED, tree.index, cell_index};
        }
        // grow
        int required_sun_points = 0;
        switch (tree.size) {
            case 0:
                required_sun_points = 1 + size_1_trees;
                break;
            case 1:
                required_sun_points = 3 + size_2_trees;
                break;
            case 2:
                required_sun_points = 7 + size_3_trees;
                break;
        }
        if (tree.size < 3 and state->my_sun >= required_sun_points)
        {
            possible_actions[index++] = (t_action){GROW, tree.index, 0};
        }
        // complete
        if (state->my_sun >= 4 and tree.size == 3)
        {
            possible_actions[index++] = (t_action){COMPLETE, tree.index, 0};
        }
    }


    int *header = (int*)ptr;
    *header = index;
    return ptr;
}

void print_possible_actions(void *array)
{
    int len = *((int*)array);
    char *sep = " ";
    t_action *possible_actions = array+sizeof(int);
    for (int i = 0; i < len; i++)
    {
        t_action action = possible_actions[i];
        switch (action.action) {
            case GROW:
                fprintf(stderr, "GROW %d%s", action.arg1, sep);
                break;
            case SEED:
                fprintf(stderr, "SEED %d %d%s", action.arg1, action.arg2, sep);
                break;
            case WAIT:
                fprintf(stderr, "WAIT%s", sep);
                break;
            case COMPLETE:
                fprintf(stderr, "COMPLETE %d%s", action.arg1, sep);
                break;
        }
    }
}

int main()
{
    // 37
    int number_of_cells;
    scanf("%d", &number_of_cells);

    t_state *initial_state = new_state();

    for (int i = 0; i < number_of_cells; i++) {
        // 0 is the center cell, the next cells spiral outwards
        int index;
        // 0 if the cell is unusable, 1-3 for usable cells
        int richness;
        // the index of the neighbouring cell for each direction
        int neigh_0;
        int neigh_1;
        int neigh_2;
        int neigh_3;
        int neigh_4;
        int neigh_5;
        scanf("%d%d%d%d%d%d%d%d", &index, &richness, &neigh_0, &neigh_1, &neigh_2, &neigh_3, &neigh_4, &neigh_5);

        // set it
        assert(index >= 0);
        assert(index <= 36);

        initial_state->cells[index] = (t_cell){index, richness};
        initial_state->len_cells++;

    }

    // game loop
    while (1) {
        // the game lasts 24 days: 0-23
        int day;
        scanf("%d", &day);
        // the base score you gain from the next COMPLETE action
        int nutrients;
        scanf("%d", &nutrients);
        // your sun points
        int sun;
        // your current score
        int score;
        scanf("%d%d", &sun, &score);
        // opponent's sun points
        int opp_sun;
        // opponent's score
        int opp_score;
        // whether your opponent is asleep until the next day
        bool opp_is_waiting;
        int _opp_is_waiting;
        scanf("%d%d%d", &opp_sun, &opp_score, &_opp_is_waiting);
        opp_is_waiting = _opp_is_waiting;
        // the current amount of trees
        int number_of_trees;
        scanf("%d", &number_of_trees);

        // set info
        initial_state->day = day;
        initial_state->my_sun = sun;
        initial_state->op_sun = opp_sun;
        initial_state->my_score = score;
        initial_state->op_score = opp_score;
        initial_state->is_op_waiting = opp_is_waiting;
        initial_state->nutrients = nutrients;
        // end 
        
        for (int i = 0; i < number_of_trees; i++) {
            // location of this tree
            int cell_index;
            // size of this tree: 0-3
            int size;
            // 1 if this is your tree
            //bool is_mine;
            // 1 if this tree is dormant
            //bool is_dormant;
            int _is_mine;
            int _is_dormant;
            scanf("%d%d%d%d", &cell_index, &size, &_is_mine, &_is_dormant);
            //is_mine = _is_mine;
            //is_dormant = _is_dormant;

            // set
            assert(cell_index >= 0);
            assert(cell_index <= 36);

            initial_state->trees[cell_index] = (t_tree){cell_index, size, _is_mine, _is_dormant};
            initial_state->len_trees++;
        }

        // all legal actions
        int number_of_possible_actions;
        scanf("%d", &number_of_possible_actions); fgetc(stdin);
        for (int i = 0; i < number_of_possible_actions; i++) {
            // try printing something from here to start with
            char possible_action[32];
            scanf("%[^\n]", possible_action); fgetc(stdin);
        }

        // Write an action using printf(). DON'T FORGET THE TRAILING \n
        // To debug: fprintf(stderr, "Debug messages...\n");


        // GROW cellIdx | SEED sourceIdx targetIdx | COMPLETE cellIdx | WAIT <message>
        printf("WAIT\n");
    }

    return 0;
}
