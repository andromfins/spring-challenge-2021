// avg: 63103.4ns
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <algorithm>
#include <sstream>
#include <chrono>
#include <unordered_map>

using namespace std;

class CubeCoord {
    std::vector<vector<int>> directions = { {+1, -1, 0}, {+1, 0, -1}, {0, +1, -1},
                                            {-1, +1, 0}, {-1, 0, +1}, {0, -1, +1}};

public:
    int x = 0;
    int y = 0;
    int z = 0;        

    CubeCoord(int x, int y, int z) 
        :x{x}, y{y}, z{z}
    {}

    bool operator==(CubeCoord b)
    {
        return x == b.x and y == b.y and z == b.z;
    }

    CubeCoord operator+(CubeCoord b)
    {
        return CubeCoord(
                x + b.x, y + b.y, z + b.z
                );
    }

    CubeCoord neighbor_with_distance(int direction, int distance)
    {
        return CubeCoord(
                x + directions[direction][0] * distance,
                y + directions[direction][1] * distance,
                z + directions[direction][2] * distance
                );
    }

    CubeCoord neighbor(int direction)
    {
        return neighbor_with_distance(direction, 1);
    }

    int distance(CubeCoord b)
    {
        return  int((abs(x - b.x) + abs(y - b.y) + abs(z - b.z)) / 2);
    }

    CubeCoord opposite()
    {
        return CubeCoord(-x, -y, -z);
    }

    vector<CubeCoord> range(int n)
    {
        vector<CubeCoord> results;
        for (int i = -n; i <= n; i++)
        {
            for (int j = max(-n, -i - n); j <= min(n, -i + n); j++)
            {
                results.push_back(CubeCoord(i, j, -i - j));
            }
        }
        return results;
    }

    vector<CubeCoord> vacinity(int n)
    {
        vector<CubeCoord> results;
        for (int i = -n; i <= n; i++)
        {
            for (int j = max(-n, -i - n); j <= min(n, -i + n); j++)
            {
                if (int((abs(i) + abs(j) + abs(-i - j)) / 2) <= 3)
                {
                    results.push_back(CubeCoord(i, j, -i - j));
                }
            }
        }
        return results;
    }

    int hashcode()
    {
        int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

};

CubeCoord index_to_cube(int index)
{
    static int board[37][3] = {
            {0, 0, 0}, {1, -1, 0}, {1, 0, -1}, {0, 1, -1},
            {-1, 1, 0}, {-1, 0, 1}, {0, -1, 1}, {2, -2, 0},
            {2, -1, -1}, {2, 0, -2}, {1, 1, -2}, {0, 2, -2},
            {-1, 2, -1}, {-2, 2, 0}, {-2, 1, 1}, {-2, 0, 2},
            {-1, -1, 2}, {0, -2, 2}, {1, -2, 1}, {3, -3, 0},
            {3, -2, -1}, {3, -1, -2}, {3, 0, -3}, {2, 1, -3},
            {1, 2, -3}, {0, 3, -3}, {-1, 3, -2}, {-2, 3, -1},
            {-3, 3, 0}, {-3, 2, 1}, {-3, 1, 2}, {-3, 0, 3},
            {-2, -1, 3}, {-1, -2, 3},{0, -3, 3}, {1, -3, 2}, {2, -3, 1}
            };
    return CubeCoord(board[index][0], board[index][1], board[index][2]);
}

int cube_to_index(CubeCoord a)
{
    static std::unordered_map<int, int> cells = {
        {29791, 0}, {30721, 1}, {30751, 2},
        {29821, 3}, {28861, 4}, {28831, 5},
        {29761, 6}, {31651, 7}, {31681, 8},
        {31711, 9}, {30781, 10}, {29851, 11}, 
        {28891, 12}, {27931, 13}, {27901, 14},
        {27871, 15}, {28801, 16}, {29731, 17},
        {30691, 18}, {32581, 19}, {32611, 20},
        {32641, 21}, {32671, 22}, {31741, 23},
        {30811, 24}, {29881, 25}, {28921, 26},
        {27961, 27}, {27001, 28}, {26971, 29},
        {26941, 30}, {26911, 31}, {27841, 32},
        {28771, 33}, {29701, 34}, {30661, 35}, {31621, 36}
    };
    return cells[a.hashcode()];
}

class Tree {
    public:
        int index = {};
        int size = {};
        bool is_mine;
        bool is_dormant;
    Tree(int index, int size, bool is_mine, bool is_dormant)
        :index{index}, size{size}, is_mine{is_mine}, is_dormant{is_dormant}
    {}
};

class Cell {
    public:
        int index;
        int richness;
    Cell(int index, int richness)
        :index{index}, richness{richness}
    {}
};

class Command {
    public:
        int command;
        int arg1;
        int arg2;
    Command(int command)
        :command{command}
    {}
    Command(int command, int index)
        :command{command}, arg1{index}
    {}
    Command(int command, int from, int to)
        :command{command}, arg1{from}, arg2{to}
    {}
};

class State {
    public:
        int day = 0;
        int my_sun = 0;
        int op_sun = 0;
        int my_score = 0;
        int op_score = 0;
        int nutrients = 0;
        bool is_op_waiting = false;

        std::unordered_map<int, Tree> trees;
        std::unordered_map<int, Cell> cells;
        std::vector<Command> possible_actions;
};

enum action_types {
		GROW,
		SEED,
		WAIT,
		COMPLETE
};

std::vector<Command> generate_plays(State state)
{
    std::vector<Command> possible_moves;

    possible_moves.push_back(Command(WAIT));

    // seed
    int seeds = 0;
    int size_1_trees = 0;
    int size_2_trees = 0;
    int size_3_trees = 0;
    for (const auto& [key, value]: state.trees)
    {
        if (value.size == 0 and value.is_mine) {
            seeds++;
        }
        else if (value.size == 1 and value.is_mine) {
            size_1_trees++;
        }
        else if (value.size == 2 and value.is_mine) {
            size_2_trees++;
        }
        else if (value.size == 3 and value.is_mine) {
            size_3_trees++;
        }
    }
    // exists X sun points , where X is the number of seeds in the forest
    for (const auto& [key, tree]: state.trees)
    {
        if (tree.is_dormant or !tree.is_mine) {
            continue;
        }
        // seed
        if (state.my_sun >= seeds)
        {
            vector<CubeCoord> vacinity = index_to_cube(tree.index).vacinity(tree.size);
            for (const auto& cube_coord: vacinity)
            {
                int cell_index = cube_to_index(cube_coord);
                // need to check if the richness is positive
                if (state.cells.at(cell_index).richness == 0)
                {
                    continue;
                }
                auto key_exists = state.trees.find(cell_index);
                // need to check if it's empty (no tree in cell)
                if (key_exists != state.trees.end()) {
                    continue;
                }
                // ok
                possible_moves.push_back(Command(SEED, tree.index, cell_index));
            }
        }
        // grow
        int required_sun_points = 0;
        if (tree.size == 0) 
        {
            required_sun_points = 1 + size_1_trees;
        }
        else if (tree.size == 1)
        {
            required_sun_points = 3 + size_2_trees;
        }
        else if (tree.size == 2)
        {
            required_sun_points = 7 + size_3_trees;
        }
        if (tree.size < 3 and state.my_sun >= required_sun_points)
        {
            possible_moves.push_back(Command(GROW, tree.index));
        }
        // complete
        if (state.my_sun >= 4 and tree.size == 3)
        {
            possible_moves.push_back(Command(COMPLETE, tree.index));
        }
    }
    return possible_moves;
}

std::vector<string> split(const std::string& s, char delimeter)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(s);
	while (std::getline(tokenStream, token, delimeter))
	{
		tokens.push_back(token);	
	}
	return tokens;
}

bool in(std::vector<int> v, int n) {
	for (int digit: v) {
		if (digit == n) {
			return true;
		}
	}
	return false;
}

int main()
{
	bool day_6 = false;
	bool day_12 = false;
	bool day_18 = false;

	std::deque<int> moves_queue = {GROW, SEED, GROW, SEED, GROW};

    // unit tests
    CubeCoord c = {0,1,-1};
    CubeCoord b = {-1,1,0};
    assert(c.x == 0 and c.y == 1 and c.z == -1);
    assert(c + b == CubeCoord(-1, 2, -1));
    assert(index_to_cube(0) == CubeCoord(0,0,0));
    assert(cube_to_index(CubeCoord(0,0,0)) == 0);
    assert(cube_to_index(CubeCoord(2,-1,-1)) == 8);
    exit(0);

    int numberOfCells; // 37
    cin >> numberOfCells; cin.ignore();
    for (int i = 0; i < numberOfCells; i++) {
        int index; // 0 is the center cell, the next cells spiral outwards
        int richness; // 0 if the cell is unusable, 1-3 for usable cells
        int neigh0; // the index of the neighbouring cell for each direction
        int neigh1;
        int neigh2;
        int neigh3;
        int neigh4;
        int neigh5;
        cin >> index >> richness >> neigh0 >> neigh1 >> neigh2 >> neigh3 >> neigh4 >> neigh5; cin.ignore();
    }

    std::vector<int> ring0 = {0,};
    std::vector<int> ring1 = {1, 2, 3, 4, 5, 6};
    std::vector<int> ring2 = {7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 16, 18};
    std::vector<int> ring3 = {19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36};
 
    // game loop
    while (1) {
        int day; // the game lasts 24 days: 0-23
        cin >> day; cin.ignore();
		/* timer */
		auto start = std::chrono::high_resolution_clock::now();

        int nutrients; // the base score you gain from the next COMPLETE action
        cin >> nutrients; cin.ignore();
        int sun; // your sun points
        int score; // your current score
        cin >> sun >> score; cin.ignore();
        int oppSun; // opponent's sun points
        int oppScore; // opponent's score
        bool oppIsWaiting; // whether your opponent is asleep until the next day
        cin >> oppSun >> oppScore >> oppIsWaiting; cin.ignore();
        int numberOfTrees; // the current amount of trees
        cin >> numberOfTrees; cin.ignore();
        for (int i = 0; i < numberOfTrees; i++) {
            int cellIndex; // location of this tree
            int size; // size of this tree: 0-3
            bool isMine; // 1 if this is your tree
            bool isDormant; // 1 if this tree is dormant
            cin >> cellIndex >> size >> isMine >> isDormant; cin.ignore();
        }
        int numberOfPossibleActions; // all legal actions
		std::vector<string> seed_possible_actions;
		std::vector<string> grow_possible_actions;
		std::vector<string> complete_possible_actions;
        cin >> numberOfPossibleActions; cin.ignore();
        for (int i = 0; i < numberOfPossibleActions; i++) {
            string possibleAction;
            getline(cin, possibleAction); // try printing something from here to start with
	    if (possibleAction[0] == 'G') {
		    grow_possible_actions.push_back(possibleAction);
	    }
	    else if (possibleAction[0] == 'S') {
		    seed_possible_actions.push_back(possibleAction);
	    }
	    else if (possibleAction[0] == 'C') {
		    complete_possible_actions.push_back(possibleAction);
	    }
        }

		/* days */
		if (!day_6 and day == 6) {
			moves_queue = {GROW, SEED, GROW, SEED, GROW, COMPLETE};
			day_6 = true;
		}
		if (!day_12 and day == 12) {
			moves_queue = {GROW, SEED, COMPLETE, GROW, COMPLETE};
			day_12 = true;
		}
		if (!day_18 and day == 18) {
			moves_queue = {GROW, COMPLETE, GROW, COMPLETE};
			day_18 = true;
		}

		bool wait = true;

		int current_wanted = moves_queue.front();
		moves_queue.pop_front();
		moves_queue.push_back(current_wanted);

		if (current_wanted == SEED && !seed_possible_actions.empty()) {
			string best = seed_possible_actions[0];
			int best_ring = -1;
			for (string seed_command: seed_possible_actions) {
				std::vector<string> parsed_command = split(seed_command, ' ');
				int to = std::stoi(parsed_command[2]);
				if (in(ring0, to)) {
					best = seed_command;
					best_ring = 0;
					break;
				}
				if (in(ring1, to) && best_ring != 0) {
					best = seed_command;	
					best_ring = 1;
				}
				if (in(ring2, to) && best_ring != 0 && best_ring != 1) {
					best = seed_command;
					best_ring = 2;
				}
			}
			cout << best << endl;
			wait = false;
		}
		if (current_wanted == GROW && !grow_possible_actions.empty()) {
			string best = grow_possible_actions[0];
			int best_ring = -1;
			for (string grow_command: grow_possible_actions) {
				std::vector<string> parsed_command = split(grow_command, ' ');
				int index = std::stoi(parsed_command[1]);	
				if (in(ring0, index)) {
					best = grow_command;
					best_ring = 0;
					break;
				}
				if (in(ring1, index) && best_ring != 0) {
					best = grow_command;
					best_ring = 1;
				}
				if (in(ring2, index) && best_ring != 0 && best_ring != 1) {
					best = grow_command;
					best_ring = 2;
				}
			}
			cout << best << endl;
			wait = false;
		}
		if (current_wanted == COMPLETE && !complete_possible_actions.empty()) {
			string best = complete_possible_actions[0];
			cout << best << endl;
			wait = false;
		}
		if (wait && !grow_possible_actions.empty()) {
			string best = grow_possible_actions[0];
			int best_ring = -1;
			for (string grow_command: grow_possible_actions) {
				std::vector<string> parsed_command = split(grow_command, ' ');
				int index = std::stoi(parsed_command[1]);	
				if (in(ring0, index)) {
					best = grow_command;
					best_ring = 0;
					break;
				}
				if (in(ring1, index) && best_ring != 0) {
					best = grow_command;
					best_ring = 1;
				}
				if (in(ring2, index) && best_ring != 0 && best_ring != 1) {
					best = grow_command;
					best_ring = 2;
				}
			}
			cout << best << endl;
			wait = false;
		}

		if (wait) {
        	cout << "WAIT" << endl;
		}

		auto end = std::chrono::high_resolution_clock::now();
		cerr << "ts: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << "ns" << std::endl;
    }
}
