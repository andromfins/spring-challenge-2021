from __future__ import annotations
from typing import List
from collections import deque
from typing import NewType
from copy import deepcopy
import random
import math
import sys


# cube coords
class CubeCoord(object):
    directions = ((+1, -1, 0), (+1, 0, -1), (0, +1, -1),
                  (-1, +1, 0), (-1, 0, +1), (0, -1, +1))

    def __init__(self, x: int, y: int, z: int):
        self.x: int = x
        self.y: int = y
        self.z: int = z

    def __equal__(self, obj: CubeCoord) -> bool:
        if self is obj:
            return True
        if (self.x != obj.x):
            return False
        if (self.y != obj.y):
            return False
        if (self.z != obj.z):
            return False
        return True

    def add(self, cube_coords: CubeCoord):
        return CubeCoord(self.x + cube_coords.x, self.y + cube_coords.y, self.z + cube_coords.z)

    def neighbor(self, direction: int) -> CubeCoord:
        return self.neighbor_with_distance(direction, 1)

    def neighbor_with_distance(self, direction: int, distance: int) -> CubeCoord:
        x, y, z = self.x, self.y, self.z
        return CubeCoord(
                    x + self.directions[direction][0] * distance,
                    y + self.directions[direction][1] * distance,
                    z + self.directions[direction][2] * distance
                )

    def distance(self, dst: CubeCoord) -> int:
        x, y, z = self.x, self.y, self.z
        return (abs(x - dst.x) + abs(y - dst.y) + abs(z - dst.z)) // 2

    def opposite(self) -> CubeCoord:
        return CubeCoord(-self.x, -self.y, -self.z)

    def range(self, n: int) -> list:
        results: List[CubeCoord] = list()
        x = -n
        while x <= n:
            y = max(-n, -x - n)
            while y <= min(n, -x + n):
                z = -x - y
                results.append(self.add(CubeCoord(x, y, z)))
                y += 1
            x += 1
        return results

    def __repr__(self):
        return "<CubeCoord: ({},{},{})>".format(self.x, self.y, self.z)
    # we can get the shaddow appliance, using distance == range(size_of_tree)

    # maybe implement ranges

def to_cell_index(cube_coords: CubeCoord) -> int:
    cells = {
        (-3, 0, 3): 31, (-3, 1, 2): 30, (-3, 2, 1): 29,
        (-3, 3, 0): 28,(-2, -1, 3): 32,(-2, 0, 2): 15,
        (-2, 1, 1): 14,(-2, 2, 0): 13,(-2, 3, -1): 27,
        (-1, -2, 3): 33,(-1, -1, 2): 16,(-1, 0, 1): 5,
        (-1, 1, 0): 4,(-1, 2, -1): 12,(-1, 3, -2): 26,
        (0, -3, 3): 34,(0, -2, 2): 17,(0, -1, 1): 6,
        (0, 0, 0): 0,(0, 1, -1): 3,(0, 2, -2): 11,
        (0, 3, -3): 25,(1, -3, 2): 35,(1, -2, 1): 18,
        (1, -1, 0): 1,(1, 0, -1): 2,(1, 1, -2): 10,
        (1, 2, -3): 24,(2, -3, 1): 36,(2, -2, 0): 7,
        (2, -1, -1): 8,(2, 0, -2): 9,(2, 1, -3): 23,
        (3, -3, 0): 19,(3, -2, -1): 20,(3, -1, -2): 21,
        (3, 0, -3): 22
    }
    return cells[(cube_coords.x, cube_coords.y, cube_coords.z)]

board = (
            (0, 0, 0), (1, -1, 0), (1, 0, -1), (0, 1, -1),
            (-1, 1, 0), (-1, 0, 1), (0, -1, 1), (2, -2, 0),
            (2, -1, -1), (2, 0, -2), (1, 1, -2), (0, 2, -2),
            (-1, 2, -1), (-2, 2, 0), (-2, 1, 1), (-2, 0, 2),
            (-1, -1, 2), (0, -2, 2), (1, -2, 1), (3, -3, 0),
            (3, -2, -1), (3, -1, -2), (3, 0, -3), (2, 1, -3),
            (1, 2, -3), (0, 3, -3), (-1, 3, -2), (-2, 3, -1),
            (-3, 3, 0), (-3, 2, 1), (-3, 1, 2), (-3, 0, 3),
            (-2, -1, 3), (-1, -2, 3),(0, -3, 3), (1, -3, 2), (2, -3, 1)
            )
 
def to_cube_coords(tree_cell_index: int) -> CubeCoord:
    board = (
            (0, 0, 0), (1, -1, 0), (1, 0, -1), (0, 1, -1),
            (-1, 1, 0), (-1, 0, 1), (0, -1, 1), (2, -2, 0),
            (2, -1, -1), (2, 0, -2), (1, 1, -2), (0, 2, -2),
            (-1, 2, -1), (-2, 2, 0), (-2, 1, 1), (-2, 0, 2),
            (-1, -1, 2), (0, -2, 2), (1, -2, 1), (3, -3, 0),
            (3, -2, -1), (3, -1, -2), (3, 0, -3), (2, 1, -3),
            (1, 2, -3), (0, 3, -3), (-1, 3, -2), (-2, 3, -1),
            (-3, 3, 0), (-3, 2, 1), (-3, 1, 2), (-3, 0, 3),
            (-2, -1, 3), (-1, -2, 3),(0, -3, 3), (1, -3, 2), (2, -3, 1)
            )
    return CubeCoord(*board[tree_cell_index])


# snippet driven development again !, hope it works

def play_move(state: dict, command: str) -> dict:
    _state = deepcopy(state)
    # simulate a single turn in a day, e.i a command
    command, *args = command.split()
    # wait
    if command == 'WAIT':
        return _state
    richness_bonus = (0, 0, 2, 4)
    # complete
    if command == 'COMPLETE':
        # TODO: add assertions, to verify requirements

        tree_index = int(args[0])
    
        # update sun points (-4)
        _state['ints'][1] -= 4
        # update score
        _state['ints'][3] += ( 
                # (nutrient)
                _state['ints'][6] + 
                richness_bonus[_state['cellsd'][tree_index][1]]
                )
        # update nutrient value
        _state['ints'][6] -= 1
        # TODO: tree delete
        _state['treesd'].pop(tree_index)

        return _state
            
    # grow
    elif command == 'GROW':
        # TODO: add assertions, to verify requirements
        tree_index = int(args[0])

        tree = _state['treesd'][tree_index]
        size_x_trees = len(list(
            filter(lambda t: t[1] == tree[1]+1 and t[2] == True, state['treesd'].values())))
        cost = (1, 3, 7)
        # update sun points
        _state['ints'][1] -= cost[tree[1]] + size_x_trees

        # update tree size
        tree[1] += 1

        # re-cast shadow
        # aslan: sun gathering is done in the begining of the day, not in the middle of
        # the turns. !

        # update tree as dormant
        tree[3] = True
        
        return _state

    # seed
    elif command == 'SEED':
        # TODO: add assertions, to verify requirements

        index0, index1 = int(args[0]), int(args[1])

        # update sun points
        seeds = len(list(filter(lambda t: t[1] == 0 and t[2] == True, state['treesd'].values())))
        _state['ints'][1] -= seeds

        # check if conflicting ? ( both players used seed in same index)

        # update tree as dormant
        _state['treesd'][index0][3] = True
        # index 1 still does not exist
        _state['treesd'][index1] = [index1, 0, True, True]

        return _state

    raise ValueError


# we need a simple bot to play agains
# or just go for max points ? that's not mcts, maybe try it, but first mcts
def bot(state: dict) -> str:
    debug = True
    # reading input

    moves_queue = deque(['COMPLETE', 'SEED','WAIT','COMPLETE', 'GROW', 'COMPLETE'])

    # initialization input
    number_of_cells = state['number_of_cells'] 
    cells = list()
    for i in range(number_of_cells):
        index, richness, *neigh = state['cells'][i] 
        cells.append((index, richness, neigh))
    if debug:
        print("cells: ", cells, file=sys.stderr)


    # input for one game turn
    day = state['day'] 
    nutrients = state['nutrients'] 
    my_sun, my_score = state['my_sun'], state['my_score']
    op_sun, op_score = state['op_sun'], state['op_score']
    op_is_waiting = state['op_is_waiting']

    number_of_trees = state['number_of_trees']
    for i in range(number_of_trees):
        cell_index, size, is_mine, is_dormant = state['trees'][i] 
        trees.append((cell_index, size, is_mine, is_dormant))

    possible_actions = state['possible_actions']

    current_wanted = moves_queue.popleft()
    moves_queue.append(current_wanted)

    # try current wanted
    wait = False
    skip = False
    for possible_action in possible_actions:
        command, *args = possible_action.split()
        if command == current_wanted:
            return possible_action
            skip = True
            break

    if possible_actions:
        return random.choice(possible_actions)

    return "WAIT"

def simulate_day(_state: dict) -> dict:
    # we have an initial number of sun points in state

    # still, we need to do the sun gathering ritual
    # next state, day += 1
    # also stop if its last day : 24


    # figure out what to do with our sun points

    # simulate it
    
    # can we do more ?

    # simulate it

    # should we stop ? it's not up to me, if im simulating through days

    # return ?
    return _state

# TODO: we need a game tree to store states in for furter mcts runs

class TreeNode:
    def __init__(self, state: dict = {} , parent: TreeNode = None, move: str = None) -> None:
        self.state: dict = state
        self.parent: TreeNode = parent
        self.root: TreeNode = parent.root
        self.children: List[TreeNode] = list()
        self.move: str = move

def simple_f(pos: TreeNode) -> float:
    # seeds + (size_of_tree * trees) + score
    # TODO: add possible sun points that can be gathered in next turn
    state = pos.state
    f = state['ints'][3]  # score
    for tree in state['treesd'].values():
        f += (tree[1] + 1)
    return f

def expand(pos: TreeNode) -> None:
    # generate possible moves
    possible_moves = generate_plays(pos.state)
    children = list()
    for possible_move in possible_moves:
        child_state = play_move(pos.state, possible_move)
        children.append(TreeNode(child_state, pos, possible_move))
    children.sort(key=simple_f,reverse=True)
    pos.children = children

def explore(pos: TreeNode) -> None:
    expand(pos)

# mcts:
# - generate plays 
# - play

# state
# - board (object)
# - day, my_sun, op_sun, my_score, op_score (int)
# - trees (list<object>)
# - possible actions (list<str>)

"""
state = {
    'board': {
        <cell_id>: CubeCoords, ? no need
    },
    'ints': [0:day, 1:my_sun, 2:op_sun, 3:my_score, 4:op_score, 5:is_op_waiting, 6:nutrients],
    'trees': [[0:cell_index, 1:size, 2:is_mine, 3:is_dormant],],
    'treesd': {
        '<cell_index>': [0:cell_index, 1:size, 2:is_mine, 3:is_dormant],
    }
    'possible_actions': [],
    'cellsd': {
        '<cell_index>': [0:cell_index, 1:richness, 2:[neigh0, neigh1, ... ]],
    },
    'cellsl': [],
}
"""

def generate_plays(state: dict) -> list: 
    # what are the possible moves ? 
    possible_moves = list()

    # we can always wait
    possible_moves.append("WAIT")

    # seed
    # exists X sun points
    #   X: number of seeds in the forest
    seeds = len(list(filter(lambda t: t[1] == 0 and t[2] == True, state['treesd'].values())))
    if state['ints'][1] >= seeds:
        # exists non dormant tree
        non_dormant_trees = filter(
                lambda t: t[1] != 0 and t[2] == True and t[3] == False, state['treesd'].values()) 
        for tree_cell_index, tree_size, _, _ in non_dormant_trees:
            # exists empty usable cell, in the seeding tree vacinity 
            vacinity = filter(
                    lambda c: c.distance(CubeCoord(0,0,0)) <= 3,
                    to_cube_coords(tree_cell_index).range(tree_size))
            for cube_coord in vacinity:
                cell_index = to_cell_index(cube_coord)
                # need to check if the richness is positive
                if state['cellsd'][cell_index][1] == 0:
                    continue
                # need to check if it's empty no (tree's in cell)
                if cell_index in state['treesd']:
                    continue
                # ok
                possible_moves.append("SEED {} {}".format(tree_cell_index, cell_index))

    # grow
    size_1_trees = len(list(
        filter(lambda t: t[1] == 1 and t[2] == True, state['treesd'].values())))
    size_2_trees = len(list(
        filter(lambda t: t[1] == 2 and t[2] == True, state['treesd'].values())))
    size_3_trees = len(list(
        filter(lambda t: t[1] == 3 and t[2] == True, state['treesd'].values())))
    # exists non dormant tree
    non_dormant_trees = filter(
            lambda t: t[1] < 3 and t[2] == True and t[3] == False, state['treesd'].values())
    sun_points = (1 + size_1_trees, 3 + size_2_trees, 7 + size_3_trees)
    for tree_cell_index, tree_size, _, _ in non_dormant_trees:
        # exists enough sun points
        #                               here tree_size is 0 (seed) with cost 1 ...
        required_sun_points = sun_points[tree_size] 
        if state['ints'][1] >= required_sun_points:
            # ok
            possible_moves.append("GROW {}".format(tree_cell_index))

    # complete
    if state['ints'][1] >= 4:
        non_dormant_trees = filter(
                #        (size == 3)      (is_mine)       (non_dormant)
                lambda t: t[1] == 3 and t[2] == True and t[3] == False, state['treesd'].values())
        for tree_cell_index, _, _, _ in non_dormant_trees:
            possible_moves.append("COMPLETE {}".format(tree_cell_index))

    return possible_moves

def perform_sun_gathering(state: dict) -> int:
    """
    They will find the shadow on a cell spooky if any of the trees casting a shadow
    is of equal or greater size than the tree on that cell.
    """
    sun_points_harvested: int = 0 
    trees = filter(   # (is_mine)
            lambda t: t[2] == True, state['treesd'].values())
    sun_direction: int = (state['ints'][0] + 3) % 6
    for tree_cell_index, tree_size, _, _ in trees:
        tree_cube_coords = to_cube_coords(tree_cell_index)
        for i in range(1,4): # (1, 2, 3)
            cell_in_sun_direction = tree_cube_coords.neighbor_with_distance(sun_direction, i)
            if cell_in_sun_direction.distance(CubeCoord(0,0,0)) > 3:
                continue
            cell_in_sun_direction_index = to_cell_index(cell_in_sun_direction)
            if cell_in_sun_direction_index in state['treesd']:
                # there is a tree
                tree_in_sun_direction = state['treesd'][cell_in_sun_direction_index]
                # does it casts a shadow ?              is it spooky ?
                # size should be equal or greater than distance i
                # it's spooky if tree in sun direction has an equal or greater tree size
                if tree_in_sun_direction[1] >= i and tree_in_sun_direction[1] >= tree_size:
                    break
        else: # all checks passed, so no spooky shadow
            sun_points_harvested += tree_size
            
    return sun_points_harvested

if __name__ == "__main__":
    # basic assertions


    debug = True
    # reading input


    # initialization input
    number_of_cells = int(input())
    cells = list()
    for i in range(number_of_cells):
        index, richness, *neigh = [int(v) for v in input().split()]
        cells.append((index, richness, neigh))
    if debug:
        print("cells: ", cells, file=sys.stderr)

    while True:

        # input for one game turn
        day = int(input())
        nutrients = int(input())
        my_sun, my_score = [int(v) for v in input().split()]
        op_sun, op_score, op_is_waiting = [int(v) for v in input().split()]

        number_of_trees = int(input())
        trees = list()
        for i in range(number_of_trees):
            cell_index, size, is_mine, is_dormant = [int(v) for v in input().split()]
            trees.append((cell_index, size, is_mine, is_dormant))
        
        number_of_possible_actions = int(input())
        for i in range(number_of_possible_actions):
            line = input()
           
        # end -- input

        # what datastructures to use, to store the input data ? and game state ?
        # for that let's define the operations we need to do
        # how to store the hex grid ?
        # mutating the game state
        #   - generate the number of possible moves
        #       - actions we can execute are:
        #           - grow <index>
        #           - seed <index0> <index1>
        #           - complete <index>
        #           - wait
        #       - what are the conditions and dependencies per action type
        #           - wait : none 
        #           - complete: 
        #               - requires 4 sun points
        #               - requires the existance of a tree of the size 3
        #           - grow:
        #               - requires X sun points, where X is game state dependent (to be calculated)
        #           - seed:
        #               ? can u seed just from size 3 threes ? or any ?
        #               - requires the existance of non dormant tree 
        #               - requires the existance of an empty usable cell, in the seeding tree vacinity
        #               - requires X sun points, where X is game state dependent (to be calculated)
        # simulating a game turn
        #   - gathering
        #   - sideeffects of actions
        #   - sun move
        #   - init ( the first turn where the game board is initialized )
        #
        # how to gathering ?
        #   - `the forest's lesser spirits will harvest sun points from each tree that is not hit
        #       by a spooky shadow` as so:
        #       - size 0 tree : 0
        #       - size 1 tree : 1
        #       - size 2 tree : 2
        #       - size 3 tree : 3
        #       thus equal to the size of the tree.
        # how to sun move ?
        #   - sun direction is day % 6 ? okay then how to apply it e.i know which cells are shaded by a tree ?
        #
        # ops:
        #   - calculate the cost of a grow action
        #
        # ? what is the nutrient value


