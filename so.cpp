#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <algorithm>
#include <sstream>
#include <chrono>
#include <unordered_map>
#include <cassert>


class Object {
    public:
        int X;
	bool check;
	std::string someVal;
	int Z;
    Object(int X, bool check, std::string someVal, int Z)
	   	:X{X} , check{check}, someVal{someVal}, Z{Z}
    {}
};

class Tree {
    public:
        int index = {};
        int size = {};
        bool is_mine;
        bool is_dormant;
    Tree(int index, int size, bool is_mine, bool is_dormant)
        :index{index}, size{size}, is_mine{is_mine}, is_dormant{is_dormant}
    {}
};


int main()
{
	Object t = Object(1, false, "foo !", 42);

	assert(t.X == 1);
	assert(t.check == false);
	assert(t.someVal == "foo !");
	assert(t.Z == 42);

	std::unordered_map<int, Object> map;

	map.insert({1, t});

	Object b = map.at(1);
	assert(b.X == 1);
	assert(b.check == false);
	assert(b.someVal == "foo !");
	assert(b.Z == 42);

	Tree tree = Tree(1, 2, true, true);

	assert(tree.index == 1);
	assert(tree.size == 2);
	assert(tree.is_mine == true);
	assert(tree.is_dormant == true);

	std::unordered_map<int, Tree> t_map;

	t_map.insert({9,tree});
	Tree T = t_map.at(9);

	assert(T.index == 1);
	assert(T.size == 2);
	assert(T.is_mine == true);
	assert(T.is_dormant == true);

}
