# avg:  175979.26984126985 ns
from __future__ import annotations
from typing import NewType
from copy import deepcopy
from collections import deque
import random
import math
import sys
import time

# rings
ring0 = (0,)
ring1 = (1, 2, 3, 4, 5, 6)
ring2 = (7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 16, 18)
ring3 = (19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36)
 
if __name__ == "__main__":

    moves_queue = deque(['GROW','SEED','GROW','SEED','GROW'])

    # initialization input
    number_of_cells = int(input())
    cells = list( )
    for i in range(number_of_cells):
        index, richness, *neigh = [int(v) for v in input().split()]
        cells.append((index, richness, neigh))
    if debug:
        print("cells: ", cells, file=sys.stderr)

    day_6 = False
    day_12 = False
    day_18 = False
    while True:

        # input for one game turn
        day = int(input())
        start = time.perf_counter()
        nutrients = int(input())
        my_sun, my_score = [int(v) for v in input().split()]
        op_sun, op_score, op_is_waiting = [int(v) for v in input().split()]

        if not day_6 and day == 6:
            moves_queue.insert(0, 'COMPLETE')
            day_6 = True
        if not day_12 and day == 6*2:
            moves_queue.insert(len(moves_queue)//2, 'COMPLETE')
            moves_queue.remove('SEED')
            day_12 = True
        if not day_18 and day == 6*3:
            moves_queue.append('COMPLETE')
            moves_queue.remove('SEED')
            day_18 = True

        number_of_trees = int(input())
        trees = list()
        for i in range(number_of_trees):
            cell_index, size, is_mine, is_dormant = [int(v) for v in input().split()]
            trees.append((cell_index, size, is_mine, is_dormant))
        
        number_of_possible_actions = int(input())
        possible_actions = list()
        seed_possible_actions = list()
        grow_possible_actions = list()
        complete_possible_actions = list()
        for i in range(number_of_possible_actions):
            line = input() 
            possible_actions.append(line)
            if line.startswith('GROW'):
                grow_possible_actions.append(line)
            elif line.startswith('SEED'):
                seed_possible_actions.append(line)
            elif line.startswith('COMP'):
                complete_possible_actions.append(line)

        if debug:print(*possible_actions, sep="\n", file=sys.stderr)
        if debug:print("queue: ", moves_queue, file=sys.stderr)
        current_wanted = moves_queue.popleft()
        moves_queue.append(current_wanted)

        # try current wanted
        wait = True

        if current_wanted == 'SEED' and seed_possible_actions:
            # pick most fertile one
            best = seed_possible_actions[0]
            best_ring = -1
            for seed_command in seed_possible_actions:
                # prioritize by checking on which ring it is ?
                _, _, to = seed_command.split()
                to = int(to)
                if to in ring0:
                    best = seed_command
                    best_ring = 0
                    break
                if to in ring1 and best_ring != 0:
                    best = seed_command
                    best_ring = 1
                if to in ring2 and best_ring != 0 and best_ring != 1:
                    best = seed_command
                    best_ring = 2
            print(best)
            wait = False

        if current_wanted == 'GROW' and grow_possible_actions:
            # pick most fertile one
            best = grow_possible_actions[0]
            best_ring = -1
            for seed_command in grow_possible_actions:
                # prioritize by checking on which ring it is ?
                _, to = seed_command.split()
                to = int(to)
                if to in ring0:
                    best = seed_command
                    best_ring = 0
                    break
                if to in ring1 and best_ring != 0:
                    best = seed_command
                    best_ring = 1
                if to in ring2 and best_ring != 0 and best_ring != 1:
                    best = seed_command
                    best_ring = 2
            print(best)
            wait = False

        if current_wanted == 'COMPLETE' and complete_possible_actions:
            print(complete_possible_actions[0])
            wait = False

        if wait and grow_possible_actions:
            # pick most fertile one
            best = grow_possible_actions[0]
            best_ring = -1
            for seed_command in grow_possible_actions:
                # prioritize by checking on which ring it is ?
                _, to = seed_command.split()
                to = int(to)
                if to in ring0:
                    best = seed_command
                    best_ring = 0
                    break
                if to in ring1 and best_ring != 0:
                    best = seed_command
                    best_ring = 1
                if to in ring2 and best_ring != 0 and best_ring != 1:
                    best = seed_command
                    best_ring = 2
            print(best)
            wait = False
 
        if wait:
            print("WAIT")   

        end = time.perf_counter()
        print("ts: ", end - start , file=sys.stderr)
        # end -- input

        # what datastructures to use, to store the input data ? and game state ?
        # for that let's define the operations we need to do
        # how to store the hex grid ?
        # mutating the game state
        #   - generate the number of possible moves
        #       - actions we can execute are:
        #           - grow <index>
        #           - seed <index0> <index1>
        #           - complete <index>
        #           - wait
        #       - what are the conditions and dependencies per action type
        #           - wait : none 
        #           - complete: 
        #               - requires 4 sun points
        #               - requires the existance of a tree of the size 3
        #           - grow:
        #               - requires X sun points, where X is game state dependent (to be calculated)
        #           - seed:
        #               ? can u seed just from size 3 threes ? or any ?
        #               - requires the existance of non dormant tree 
        #               - requires the existance of an empty usable cell, in the seeding tree vacinity
        #               - requires X sun points, where X is game state dependent (to be calculated)
        # simulating a game turn
        #   - gathering
        #   - sideeffects of actions
        #   - sun move
        #   - init ( the first turn where the game board is initialized )
        #
        # how to gathering ?
        #   - `the forest's lesser spirits will harvest sun points from each tree that is not hit
        #       by a spooky shadow` as so:
        #       - size 0 tree : 0
        #       - size 1 tree : 1
        #       - size 2 tree : 2
        #       - size 3 tree : 3
        #       thus equal to the size of the tree.
        # how to sun move ?
        #   - sun direction is day % 6 ? okay then how to apply it e.i know which cells are shaded by a tree ?
        #
        # ops:
        #   - calculate the cost of a grow action
        #
        # ? what is the nutrient value


